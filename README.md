# geofence

### Requirements
1. Xcode 11 and above
2. iOS 13 supported devices

### Compile and build project file instruction

1. Create Indentifier on Certificates, Identifiers & Profiles from Apple Developer (https://developer.apple.com/)
2. Enable Access WiFi Information and Push Notifications configurations
3. Build and run the project

