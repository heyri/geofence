//
//  AddGeofenceViewController.swift
//  Geofencing
//
//  Created by Muhammad Heyrizuan Samsudin on 04/02/2020.
//  Copyright © 2020 Heyrizuan Samsudin. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol AddGeofenceViewControllerDelegate {
    func addGeofenceViewController(_ controller: AddGeofenceViewController, didAddCoordinate coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, wifiNetwork: String)
}


class AddGeofenceViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var radiusTextField: UITextField!
    @IBOutlet weak var wifiSSIDLabel: UILabel!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var wifiSSID: String = ""
    var wifiEnable: Bool = false
    var locationManager = CLLocationManager()
    var delegate: AddGeofenceViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkLocationEnable()
        doneButton.isEnabled = false
        // Do any additional setup after loading the view.
    }
    
    func checkLocationEnable(){
        let status = CLLocationManager.authorizationStatus()
        if status == .authorizedWhenInUse || status == .authorizedAlways{
            getWifiNetwork()
            zoomToUser()
        } else {
            locationManager.delegate = self
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func zoomToUser(){
        if let userLocation = locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 100, longitudinalMeters: 100)
            mapView.setRegion(viewRegion, animated: false)
        }
    }
    
    
    func getWifiNetwork(){
        
        self.wifiSSID = Utilities.sharedInstance.getWiFiSsid() ?? ""
        
        if self.wifiSSID == "" {
            let wifiNotifcation = UIAlertController(title: "Please Connect to Wi-Fi", message: "Please connect to your standard Wi-Fi Network", preferredStyle: .alert)
            wifiNotifcation.addAction(UIAlertAction(title: "Open Wi-Fi", style: .default, handler: { (nil) in
                let url = URL(string: "App-Prefs:root=WIFI")
                
                if UIApplication.shared.canOpenURL(url!){
                    UIApplication.shared.open(url!)
                    self.navigationController?.popViewController(animated: false)
                }
            }))
            self.present(wifiNotifcation, animated: true, completion: nil)
        } else {
            self.wifiSSIDLabel.text = self.wifiSSID
            self.wifiEnable = true
        }
    }
    
    @IBAction func doneTouch(_ sender: UIBarButtonItem) {
        let coordinate = mapView.centerCoordinate
        let radius = Double(radiusTextField.text!) ?? 0
        let identifier = NSUUID().uuidString
        delegate?.addGeofenceViewController(self, didAddCoordinate: coordinate, radius: radius, identifier: identifier, wifiNetwork: self.wifiSSID)
    }
    
    @IBAction func editingRadius(_ sender: UITextField) {
        doneButton.isEnabled = !radiusTextField.text!.isEmpty && wifiEnable
    }
    
    @IBAction func cancelTouch(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
        
}


extension AddGeofenceViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        mapView.showsUserLocation = status == .authorizedAlways
        getWifiNetwork()
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with the following error: \(error)")
    }
    
}
