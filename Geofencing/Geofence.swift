//
//  Geofence.swift
//  Geofencing
//
//  Created by Muhammad Heyrizuan Samsudin on 02/02/2020.
//  Copyright © 2020 Heyrizuan Samsudin. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

class Geofence: NSObject, Codable, MKAnnotation {
    
    enum CodingKeys: String, CodingKey {
        case latitude, longitude, radius, identifier, wifiNetwork
    }
    
    var coordinate: CLLocationCoordinate2D
    var radius: CLLocationDistance
    var identifier: String
    var wifiNetwork: String
    
    var title: String? {
      if wifiNetwork.isEmpty {
        return "No Title"
      }
      return wifiNetwork
    }
    
    var subtitle: String? {
        return "Radius: \(radius)m"
    }
    
    init(coordinate: CLLocationCoordinate2D, radius: CLLocationDistance, identifier: String, wifiNetwork: String) {
        self.coordinate = coordinate
        self.radius = radius
        self.identifier = identifier
        self.wifiNetwork = wifiNetwork
        
    }
    
    // MARK: Codable
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let latitude = try values.decode(Double.self, forKey: .latitude)
        let longitude = try values.decode(Double.self, forKey: .longitude)
        coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        radius = try values.decode(Double.self, forKey: .radius)
        identifier = try values.decode(String.self, forKey: .identifier)
        wifiNetwork = try values.decode(String.self, forKey: .wifiNetwork)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(coordinate.latitude, forKey: .latitude)
        try container.encode(coordinate.longitude, forKey: .longitude)
        try container.encode(radius, forKey: .radius)
        try container.encode(identifier, forKey: .identifier)
        try container.encode(wifiNetwork, forKey: .wifiNetwork)
    }
    
}

extension Geofence {
  public class func allGeofences() -> [Geofence] {
    guard let savedData = UserDefaults.standard.data(forKey: preferencesKeys.savedGeofences) else { return [] }
    let decoder = JSONDecoder()
    if let savedGeofences = try? decoder.decode(Array.self, from: savedData) as [Geofence] {
      return savedGeofences
    }
    return []
  }
}
