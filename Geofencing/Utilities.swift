//
//  Utilities.swift
//  Geofencing
//
//  Created by Muhammad Heyrizuan Samsudin on 04/02/2020.
//  Copyright © 2020 Heyrizuan Samsudin. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration.CaptiveNetwork

class Utilities: NSObject {

    class var sharedInstance : Utilities {
        struct Static {
            static let instance : Utilities = Utilities()
        }
        return Static.instance
    }
    
    func getWiFiSsid() -> String? {
          var ssid: String?
          if let interfaces = CNCopySupportedInterfaces() as NSArray? {
              for interface in interfaces {
                  if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                      ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
                      break
                  }
              }
          }
          return ssid
      }
    
}

extension UIViewController {
  func showAlert(withTitle title: String?, message: String?) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
    alert.addAction(action)
    present(alert, animated: true, completion: nil)
  }
}
