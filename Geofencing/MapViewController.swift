//
//  ViewController.swift
//  Geofencing
//
//  Created by Muhammad Heyrizuan Samsudin on 02/02/2020.
//  Copyright © 2020 Heyrizuan Samsudin. All rights reserved.
//

import UIKit
import MapKit
import SystemConfiguration.CaptiveNetwork
import CoreLocation

struct preferencesKeys {
    static let savedGeofences = "savedGeofences"
}

class MapViewController: UIViewController {
    
    var geofences: [Geofence] = []
    var wifiSSID : String = ""
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadAllGeotifications()
        
        let monitoredRegions = locationManager.monitoredRegions

        for region in monitoredRegions{
            locationManager.stopMonitoring(for: region)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkLocationEnable()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addGeofence" {
            let navigationController = segue.destination as! UINavigationController
            let vc = navigationController.viewControllers.first as! AddGeofenceViewController
            vc.delegate = self
        }
    }
    
    func loadAllGeotifications() {
        geofences.removeAll()
        let allGeofences = Geofence.allGeofences()
        allGeofences.forEach { add($0) }
    }
    
    func add(_ geofence: Geofence) {
        geofences.append(geofence)
        mapView.addAnnotation(geofence)
        addRadiusOverlay(forGeofence: geofence)
    }
    
    func remove(_ geofence: Geofence) {
        guard let index = geofences.firstIndex(of: geofence) else { return }
        geofences.remove(at: index)
        mapView.removeAnnotation(geofence)
        removeRadiusOverlay(forGeofence: geofence)
    }
    
    func checkLocationEnable(){
        let status = CLLocationManager.authorizationStatus()
        if status == .authorizedAlways{
            getWifiNetwork()
        } else {
            locationManager.delegate = self
            locationManager.requestAlwaysAuthorization()
            getWifiNetwork()
            //locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func getWifiNetwork(){
        
        self.wifiSSID = Utilities.sharedInstance.getWiFiSsid() ?? ""
        title = self.wifiSSID
        
        if self.wifiSSID == "" {
            let wifiNotifcation = UIAlertController(title: "Please Connect to Wi-Fi", message: "Please connect to your standard Wi-Fi Network", preferredStyle: .alert)
            wifiNotifcation.addAction(UIAlertAction(title: "Open Wi-Fi", style: .default, handler: { (nil) in
                let url = URL(string: "App-Prefs:root=WIFI")
                
                if UIApplication.shared.canOpenURL(url!){
                    UIApplication.shared.open(url!)
                    self.navigationController?.popViewController(animated: false)
                }
            }))
            self.present(wifiNotifcation, animated: true, completion: nil)
        } else {
            setUpGeofence()
        }
        
    }
    
    @IBAction func getCurrentLocation(_ sender: UIBarButtonItem) {
        checkLocationEnable()
    }
    
    
    func setUpGeofence(){
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.pausesLocationUpdatesAutomatically = false
        self.locationManager.activityType = .otherNavigation
        
        //Zoom to user location
        if let userLocation = locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 100, longitudinalMeters: 100)
            mapView.setRegion(viewRegion, animated: false)
            mapView.showsUserLocation = true
        }
        
        DispatchQueue.main.async {
            self.locationManager.startUpdatingLocation()
            
        }
        
        for geofence in geofences {
            if geofence.wifiNetwork == self.wifiSSID {
                startMonitoring(geofence: geofence)
                
            }
        }
    }
        
    func addRadiusOverlay(forGeofence geofence: Geofence) {
        mapView?.addOverlay(MKCircle(center: geofence.coordinate, radius: geofence.radius))
    }
    
    func removeRadiusOverlay(forGeofence geofence: Geofence) {
        // Find exactly one overlay which has the same coordinates & radius to remove
        guard let overlays = mapView?.overlays else { return }
        for overlay in overlays {
            guard let circleOverlay = overlay as? MKCircle else { continue }
            let coord = circleOverlay.coordinate
            if coord.latitude == geofence.coordinate.latitude && coord.longitude == geofence.coordinate.longitude && circleOverlay.radius == geofence.radius {
                mapView?.removeOverlay(circleOverlay)
                break
            }
        }
    }
    
    func region(with geofence: Geofence) -> CLCircularRegion {
        let region = CLCircularRegion(center: geofence.coordinate, radius: geofence.radius, identifier: geofence.identifier)
        region.notifyOnEntry = true
        region.notifyOnExit = true
        return region
    }
    
    func startMonitoring(geofence: Geofence) {
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            showAlert(withTitle:"Error", message: "Geofencing is not supported on this device!")
            return
        }
        
        let status = CLLocationManager.authorizationStatus()
        if  status == .authorizedAlways{
            let fenceRegion = region(with: geofence)
            locationManager.startMonitoring(for: fenceRegion)
        } else {
            showAlert(withTitle:"Warning", message: "Please grant apps to access your iPhone’s location to notify you when you enter or leave a geofence.")
        }
        
    }
    
    func stopMonitoring(geofence: Geofence) {
        for region in locationManager.monitoredRegions {
            guard let circularRegion = region as? CLCircularRegion, circularRegion.identifier == geofence.identifier else { continue }
            locationManager.stopMonitoring(for: circularRegion)
        }
    }
    
    func saveAllGeofences() {
       let encoder = JSONEncoder()
       do {
         let data = try encoder.encode(geofences)
        UserDefaults.standard.set(data, forKey: preferencesKeys.savedGeofences)
       } catch {
         print("error encoding geotifications")
       }
     }
    
}

// MARK: - Location Manager Delegate
extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        getWifiNetwork()
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with the following error: \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
           if region is CLCircularRegion {
            for geofence in geofences {
                if self.wifiSSID != geofence.wifiNetwork && geofence.identifier == region.identifier {
                    showAlert(withTitle:"Notice", message: "You're exiting \(geofence.wifiNetwork)")
                    stopMonitoring(geofence: geofence)
                }
            }
                
           }
       }
       
       // called when user Enters a monitored region
       func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
           if region is CLCircularRegion {
               showAlert(withTitle:"Notice", message: "Entering Geofence area")
           }
       }
    
}

extension MapViewController: AddGeofenceViewControllerDelegate {
  
    func addGeofenceViewController(_ controller: AddGeofenceViewController, didAddCoordinate coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, wifiNetwork: String) {
        controller.dismiss(animated: true, completion: nil)
        let clampedRadius = min(radius, locationManager.maximumRegionMonitoringDistance)
        let geofence = Geofence(coordinate: coordinate, radius: clampedRadius, identifier: identifier, wifiNetwork: wifiNetwork)
        add(geofence)
        saveAllGeofences()
        startMonitoring(geofence: geofence)
        
  }
  
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
      let identifier = "myGeofence"
      if annotation is Geofence {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
        if annotationView == nil {
          annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
          annotationView?.canShowCallout = true
          let removeButton = UIButton(type: .custom)
          removeButton.frame = CGRect(x: 0, y: 0, width: 23, height: 23)
          removeButton.setImage(UIImage(named: "delete")!, for: .normal)
          annotationView?.leftCalloutAccessoryView = removeButton
        } else {
          annotationView?.annotation = annotation
        }
        return annotationView
      }
      return nil
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            circleRenderer.strokeColor = .green
            circleRenderer.fillColor = UIColor.green.withAlphaComponent(0.4)
            return circleRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
      // Delete geotification
      let geofence = view.annotation as! Geofence
      remove(geofence)
      saveAllGeofences()
    }
    
}


